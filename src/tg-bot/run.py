import subprocess
import json
import atexit
import signal
import sys
import threading

import click


def get_ngrok_url():
    """Start ngrok and capture a public URL it created for us. Shut down ngrok upon the termination of the program."""

    ngrok_cmd = "ngrok http 8080 --log=stdout --log-format=json --log-level=info"
    process = subprocess.Popen(ngrok_cmd.split(), stdout=subprocess.PIPE)

    def close_tunnel():
        print("\nClosing ngrok tunnel...")
        process.terminate()

    # register function for this process to be called upon termination of program (i.e. at exit)
    atexit.register(
        close_tunnel
    )  # despite below handlers, might still come in handy, e.g., if exception is triggered or smth else causes program termination

    # Register a custom signal handler
    def signal_handler(sig, frame):
        close_tunnel()
        sys.exit(0)

    # Register handlers for common signals we may receive
    signal.signal(signal.SIGINT, signal_handler)  # Handle Ctrl+C
    signal.signal(signal.SIGTERM, signal_handler)  # Handle termination requests
    signal.signal(signal.SIGHUP, signal_handler)  # Handle terminal closure
    signal.signal(signal.SIGQUIT, signal_handler)  # Handle Ctrl+\

    while True:
        line = process.stdout.readline()
        if not line:
            break
        line_json = json.loads(line)
        if "msg" in line_json and line_json["msg"].startswith("started tunnel"):
            return line_json["url"]


# Function to handle logs
def handle_log_output(stream):
    while True:
        line = stream.readline()
        if not line:
            break
        print(line.strip())


@click.command()
@click.option(
    '--batch_size',
    default=5,
    type=click.IntRange(min=3, max=15),
    help='Number of funds to show each time after /start or /more is executed (between 3 and 15)'
)
def main(batch_size):
    url = get_ngrok_url()
    print("ngrok url:", url)

    # Start the TG bot as a subprocess and capture stdout and stderr
    process = subprocess.Popen(
        [
            "python",
            "-u",
            "tg_bot.py",
            url,
            "--batch_size",
            str(batch_size),
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )

    # Create threads to handle stdout and stderr logs
    stdout_thread = threading.Thread(target=handle_log_output, args=(process.stdout,))
    stderr_thread = threading.Thread(target=handle_log_output, args=(process.stderr,))

    # Start the threads
    stdout_thread.start()
    stderr_thread.start()

    # Wait for the TG bot subprocess to finish
    process.wait()

    # Wait for the log handling threads to finish
    stdout_thread.join()
    stderr_thread.join()


# Call the main function when this script is executed
if __name__ == "__main__":
    main()