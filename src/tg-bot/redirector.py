"""
This script implements a simple Flask web application that tracks user clicks and logs them to a CSV file.
The application listens for incoming HTTP GET requests on the `/redirect` route with `user_id`, `action`, and `url` query parameters.
Upon receiving a request, it logs the click information (timestamp, user_id, action, and url) to a CSV file and redirects the user to the target URL.
"""

from flask import Flask, request, redirect
import csv
import os
from datetime import datetime

app = Flask(__name__)

LOG_FILE = "click_tracking.csv"


@app.route("/redirect")
def handle_redirect():
    user_id = request.args.get("user_id")
    action = request.args.get("action")
    url = request.args.get("url")

    # Get the current timestamp in UTC format
    timestamp = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

    # Write CSV header if the file doesn't exist yet
    if not os.path.exists(LOG_FILE):
        with open(LOG_FILE, mode="w", newline="", encoding="utf-8") as f:
            csv_writer = csv.writer(f)
            csv_writer.writerow(["timestamp", "user_id", "action", "url"])

    # Log the click information to a CSV file
    with open(LOG_FILE, mode="a", newline="", encoding="utf-8") as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow([timestamp, user_id, action, url])

    # Redirect the user to the target URL
    return redirect(url)


if __name__ == "__main__":
    app.run(port=8080, debug=False)
