import io
import os
import sys
import threading
import time
from pathlib import Path
from typing import List

from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())

BOT_TOKEN = os.environ.get("TELEGRAM_RECSYS_BOT_TOKEN")
assert (
    BOT_TOKEN
), "Bot token not found. Make sure TELEGRAM_RECSYS_BOT_TOKEN env variable is set"

SRC_DIR = str(Path(__file__).resolve().parent.parent)
sys.path.append(SRC_DIR)  # enables doing `from models.model ...` below

import click
import pandas as pd
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import (CallbackContext, CallbackQueryHandler,
                          CommandHandler, Updater)

from models.model import *
import s3_utils
import utils

PROJ_ROOT_DIR: Path = utils.get_project_root()
DATA_DIR = PROJ_ROOT_DIR / "data" / "raw"
assert DATA_DIR.exists()

BASE_REDIRECTION_URL = None  # will be provided by ngrok
SHOW_IN_BATCHES_OF_SIZE = (
    None  # how many funds to show each time after /start or /more is executed
)

donate_to = None  # will be set either to 'funds' or 'individuals'
selected_topic = None
shown_batches_counter = (
    0  # for counting batches of funds already shown for currently selected topic
)
ranked_fund_indices = None

# Define the list of items
ITEMS = [
    "Помощь людям с онкологией и пациентам хосписов",
    "Помощь сиротам",
    "Помощь животным",
]

NUZHNA_POMOSH_FILE_NAME = "df_nuzhnapomosh.csv"
DOBRO_MAIL_FILE_NAME = "df_tosha.csv"

# TODO: fetch from s3 instead
df = pd.read_csv(DATA_DIR / NUZHNA_POMOSH_FILE_NAME)
df_mail = pd.read_csv(DATA_DIR / DOBRO_MAIL_FILE_NAME)

UPDATE_INTERVAL = 10  # Check for updates every 60 seconds
data = {
    DOBRO_MAIL_FILE_NAME: {
        'df': None,
        'current_file_hash': None,
        'update_ready': False
    }, 
    NUZHNA_POMOSH_FILE_NAME: {
        'df': None,
        'current_file_hash': None,
        'update_ready': False
    }
}


def start(update: Update, context: CallbackContext):
    """
    Handle the /start command. Show a choice between donating to funds or individuals.
    """
    print(f"Got /start from user {update.message.from_user.id}")

    global df, df_mail
    
    for file_name in data.keys():
        if data[file_name]['update_ready']:
            # TODO: only should be performed for users that press start, i.e., it should not be performed globally for all users
            if file_name == DOBRO_MAIL_FILE_NAME:
                df_mail = data[file_name]['df']
            else:
                df = data[file_name]['df']
            data[file_name]['update_ready'] = False
        print(f'UPDATED DATA for file {file_name} IN START')

    message = (
        f"Кол-во известных фондов: {len(df)}. Кол-во известных благпроектов: {len(df_mail)}"
    )
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)

    keyboard = [
        [InlineKeyboardButton("Фондам", callback_data="donate_funds")],
        [
            InlineKeyboardButton(
                "Конкретным людям или животным", callback_data="donate_individuals"
            )
        ],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    message = (
        "Выберите, хотите ли вы пожертвовать фондам или конкретным людям и животным:"
    )
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=message, reply_markup=reply_markup
    )


def show_topics(update: Update, context: CallbackContext):
    """
    Show a list of available topics for the user to select from.
    """
    # print(f'Got /topics from user {update.message.from_user.id}')
    keyboard = [
        [InlineKeyboardButton(f"{i+1}. {item}", callback_data=str(i + 1))]
        for i, item in enumerate(ITEMS)
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    message = "Пожалуйста, выберите интересующую вас тему пожертвований из списка:"
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=message, reply_markup=reply_markup
    )


def more(update: Update, context: CallbackContext):
    """
    Handle the /more command. Show more funds related to the currently selected topic.
    """
    global shown_batches_counter

    print(f"Got /more from user {update.message.from_user.id}")

    if selected_topic is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Тема пожертвований еще не была выбрана. Чтобы ее выбрать, введите /start",
        )
        return

    start_batch_ix = shown_batches_counter * SHOW_IN_BATCHES_OF_SIZE
    if start_batch_ix >= len(ranked_fund_indices):
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Фонды по этой теме закончились. Чтобы выбрать новую тему, введите /start",
        )
        return
    end_batch_ix = min(
        (shown_batches_counter + 1) * SHOW_IN_BATCHES_OF_SIZE, len(ranked_fund_indices)
    )

    indices = ranked_fund_indices[start_batch_ix:end_batch_ix]

    if donate_to == "funds":
        display_funds(indices, update, context)
    else:
        display_donations_to_individuals(indices, update, context)
    shown_batches_counter += 1
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Команды: \n/more - показать еще фонды/проекты по этой теме\n/start - начать сначала",
    )


def handle_callback_query(update: Update, context: CallbackContext):
    """
    Handle callback queries from inline keyboards (buttons). Set the selected topic and show related funds or individuals.
    """
    global selected_topic, shown_batches_counter, ranked_fund_indices, donate_to

    query = update.callback_query
    data = query.data

    if data in ("donate_funds", "donate_individuals"):
        donate_to = "funds" if data == "donate_funds" else "individuals"
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f'Вы выбрали жертвовать {"фондам" if data == "donate_funds" else "конкретным людям или животным"}.',
        )
        show_topics(update, context)
    else:
        shown_batches_counter = 0
        index = int(data) - 1
        selected_topic = ITEMS[index]

        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f'Вы выбрали тему "{selected_topic}". По этой теме вас могут заинтересовать след. {"фонды" if donate_to == "funds" else "программы пожертвования"}:',
        )

        indices = get_ranked_fund_indices(selected_topic, kind=donate_to)
        ranked_fund_indices = indices  # save globally
        indices = indices[:SHOW_IN_BATCHES_OF_SIZE]

        if donate_to == "funds":
            display_funds(indices, update, context)
        else:
            display_donations_to_individuals(indices, update, context)

        shown_batches_counter += 1
        query.edit_message_reply_markup(reply_markup=None)
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Команды: \n/more - показать еще фонды/проекты по этой теме\n/start - начать сначала",
        )


def display_funds(indices: List[int], update: Update, context: CallbackContext):
    """
    Display a list of funds to the user based on the provided indices.
    """
    user_id = update.effective_user.id
    for ix in indices:
        name, descr, amount_raised, donate_link, more_info_link = df.iloc[ix]

        donate_redirect_url = f"{BASE_REDIRECTION_URL}?user_id={user_id}&action=donate_{ix}&url={donate_link}"
        more_info_redirect_url = f"{BASE_REDIRECTION_URL}?user_id={user_id}&action=more_info_{ix}&url={more_info_link}"

        message = f'Фонд "{name}": {descr}'
        keyboard = [
            [
                InlineKeyboardButton("Пожертвовать", url=donate_redirect_url),
                InlineKeyboardButton("Подробнее о фонде", url=more_info_redirect_url),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=message, reply_markup=reply_markup
        )


def display_donations_to_individuals(
    indices: List[int], update: Update, context: CallbackContext
):
    """
    Display a list of funds to the user based on the provided indices.
    """
    user_id = update.effective_user.id
    for ix in indices:
        # name, descr, amount_raised, donate_link, more_info_link = df.iloc[ix]
        name, descr, location, amount_raised, amount_need, donate_link = df_mail.iloc[
            ix
        ]
        more_info_link = donate_link[: -len("/help/money/")]

        donate_redirect_url = f"{BASE_REDIRECTION_URL}?user_id={user_id}&action=donate_{ix}&url={donate_link}"
        more_info_redirect_url = f"{BASE_REDIRECTION_URL}?user_id={user_id}&action=more_info_{ix}&url={more_info_link}"

        message = f'Проект "{name}": {descr}\nЦель: {amount_need} руб.\nСобрано: {amount_raised} руб.\nОсталось: {amount_need - amount_raised} руб.'
        keyboard = [
            [
                InlineKeyboardButton("Пожертвовать", url=donate_redirect_url),
                InlineKeyboardButton("Подробнее", url=more_info_redirect_url),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=message, reply_markup=reply_markup
        )


@click.command()
@click.argument("base_redirection_url")
@click.option(
    '--batch_size',
    default=5,
    type=click.IntRange(min=3, max=15),
    help='Number of funds to show each time after /start or /more is executed (between 3 and 15)'
)
def main(base_redirection_url, batch_size):
    """
    Main function to start the Telegram bot. Adds handlers for /start and /more commands and callback queries.
    """
    global SHOW_IN_BATCHES_OF_SIZE, BASE_REDIRECTION_URL

    SHOW_IN_BATCHES_OF_SIZE = batch_size
    BASE_REDIRECTION_URL = base_redirection_url + "/redirect"

    print("=== STARTING TG BOT ===")

    updater = Updater(token=BOT_TOKEN, use_context=True)

    updater.dispatcher.add_handler(CommandHandler("start", start))
    updater.dispatcher.add_handler(CommandHandler("more", more))
    updater.dispatcher.add_handler(CallbackQueryHandler(handle_callback_query))

    updater.start_polling()
    updater.idle()


def update_data():  # TODO: allow different check intervals for different data sources (nuzhna pomosh should be checked less frequently)
    """Update the used data for each file if it was updated in the object storage."""
    global data
    while True:
        for file_name in data.keys():
            res = s3_utils.fetch_and_return_if_updated(file_name, data[file_name]['current_file_hash'])
            if res is not None:
                data[file_name]["df"] = pd.read_csv(io.BytesIO(res[0]))
                data[file_name]["current_file_hash"] = res[1]
                data[file_name]["update_ready"] = True
        time.sleep(UPDATE_INTERVAL)


if __name__ == "__main__":
    # Start the data update thread
    update_thread = threading.Thread(target=update_data, daemon=True)
    update_thread.start()

    main()
