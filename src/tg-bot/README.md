### How to run the bot (currently, it's advised to use conda):
1. Create a conda env with Python 3.8.13.
2. Install the requirements.
3. Make sure the bot token is set as an env var (or simply paste it in the code).
4. Sign up for ngrok and set your user token with `ngrok config add-authtoken <your token>`.
5. Activate your conda env.
6. In one terminal, run `python redirector.py`.
7. In another one, run `python run.py`.

### Why using a redirector?
The Telegram Bot API [prevents](https://core.telegram.org/bots/api#inlinekeyboardbutton) tracking user clicks/taps on buttons containing URLs, one can either have buttons that take users to a given URL upon clicking (`url` param) OR buttons, clicking on which is trackable by receiving an update query in the code (`callback_data` param), but not both. An alternative we use to work around that is giving the user a link to a service that would record what button (i.e., for which fund) they have clicked and then auto-redirect them to the actual target page. In this regard, ngrok is needed to enable using a locally deployed Flask redirector app, since "localhost" links are not permitted.
