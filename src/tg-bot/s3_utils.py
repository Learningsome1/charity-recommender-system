import hashlib
import os
from typing import Tuple

import boto3
from dotenv import find_dotenv, load_dotenv


load_dotenv(find_dotenv())

# Your Yandex Object Storage credentials
YANDEX_CLOUD_SERVICE_ACCOUNT_ACCESS_KEY = os.environ['YANDEX_CLOUD_SERVICE_ACCOUNT_ACCESS_KEY']
YANDEX_CLOUD_SERVICE_ACCOUNT_SECRET_KEY = os.environ['YANDEX_CLOUD_SERVICE_ACCOUNT_SECRET_KEY']

S3_BUCKET_NAME = os.environ['S3_BUCKET_NAME']

# Create a Boto3 session
session = boto3.session.Session()

# Create a Yandex Object Storage client
s3 = session.client(
    service_name='s3',
    aws_access_key_id=YANDEX_CLOUD_SERVICE_ACCOUNT_ACCESS_KEY,
    aws_secret_access_key=YANDEX_CLOUD_SERVICE_ACCOUNT_SECRET_KEY,
    endpoint_url='https://storage.yandexcloud.net',
)


def fetch_and_return_if_updated(file_name: str, current_file_hash: str) -> Tuple[bytes, str]:
    print(f'Trying to fetch file "{file_name}" from object storage and check if it has changed...')

    # Get the CSV file from the S3 bucket
    try:
        file_ = s3.get_object(Bucket=S3_BUCKET_NAME, Key=file_name)
        # Calculate the hash of the new file
        data: bytes = file_['Body'].read()
        new_file_hash = hashlib.md5(data).hexdigest()
        # Check if the file hash is different from the current one
        if current_file_hash != new_file_hash:
            print('File has changed, returning new version of it...')
            return data, new_file_hash
        else:
            print(f'File hasn\'t changed')
    except Exception as e:
        print(f"Error occurred when fetching file from object storage and trying to get its hash: {e}")
        return