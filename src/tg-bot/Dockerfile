FROM python:3.8.13-slim-bullseye

# These lang settings are probably unessential, taken from here: https://github.com/zironycho/pytorch-docker/blob/main/1.12.0/Dockerfile
RUN export DEBIAN_FRONTEND=noninteractive \
  && echo "LC_ALL=en_US.UTF-8" >> /etc/environment \
  && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
  && echo "LANG=en_US.UTF-8" > /etc/locale.conf \
  && apt update && apt install -y locales \
  && locale-gen en_US.UTF-8 \
  && rm -rf /var/lib/apt/lists/*

ENV LANG=en_US.UTF-8 \
  LANGUAGE=en_US:en \
  LC_ALL=en_US.UTF-8

# Keeping these lines here enables us to take advantage of layer caching: installing these
# packages in a separate RUN command creates a separate Docker image layer, which can be cached by 
# Docker. If no changes are made to these specific lines in the Dockerfile, Docker will use the cached 
# layer, speeding up subsequent builds
RUN pip install \
  torch==1.12.1+cpu \
  torchvision==0.13.1+cpu \
  --extra-index-url https://download.pytorch.org/whl/cpu \
  && rm -rf /root/.cache/pip

# Set the working directory
WORKDIR /app

# Install packages (again, for sake of using layer caching, this time when only code changes take place)
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . .

# Expose the port that container will listen on (not needed for our TG bot)
# EXPOSE 5000

# Set the command to run when the container starts. Note that container will take a while to start due to
# downloading model weights in model.py
CMD ["python", "tg_bot.py"]
