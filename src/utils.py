import os
from pathlib import Path


def get_project_root():
    current_file_path = Path(__file__).resolve()
    # Assuming your utility file is in src/utils, go up two levels to reach the root folder
    root_folder = current_file_path.parents[1]
    return root_folder
