import pandas as pd
import os
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import requests
from bs4 import BeautifulSoup

def nuzhnapomosh_fund_info(fund) -> str:
    """
    Функция возвращает информацию о благотворительных фондах.

    Args:
        fund (bs4.element.Tag): HTML-элемент, содержащий тег, атрибуты и 
        содержимое о фонде на странице nuzhnapomosh.ru.

    Returns:
        str: название фонда, описание, регион, 
             всего собрано средств, осталось собрать, 
             ссылка на проект.
    """

    name = fund.find("p", class_="np-card__title").text.strip()
    description = fund.find("p", class_="np-card__descr").text.strip()
    location = fund.find_all("span", class_="np-card__locate-txt")[0].text.strip()

    donate_button_link = "https://nuzhnapomosh.ru" + fund.find(
        "a", class_="np-card__info-button"
    ).get("href")

    try:
        resp = requests.get(donate_button_link[:-5])
        soup = BeautifulSoup(resp.text, "html.parser")
        amount_raised = soup.find_all(
            "span", class_="b-fund-single-help__progress-current b-rub-after"
        )[0].text.replace(" ", "")
        amount_raised = int(amount_raised)
        amount_need = soup.find_all(
            "span", class_="b-fund-single-help__progress-target b-rub-after"
        )[0].text.replace(" ", "")
        amount_need = int(amount_need)

    except:
        amount_raised = None
        amount_need = None

    return name, description, location, amount_raised, amount_need, donate_button_link

chrome_options = Options()
chrome_options.add_argument("--headless=new")
driver = webdriver.Chrome("./chromedriver", chrome_options=chrome_options)
LINK = "https://nuzhnapomosh.ru/funds/"
driver.get(LINK)
BUTTON = ".b-funds__more-button.np-button.np-button_more.np-button_big.np-button_block.np-button_border-black.js-load-more"

while True:
    try:
        btn = driver.find_element(By.CSS_SELECTOR, BUTTON)
        btn.click()
        time.sleep(5)
    except:
        break
response = driver.page_source
driver.quit()

soup = BeautifulSoup(response, "html.parser")
fund_list = soup.find_all("div", class_="np-card__inner")

results = []

for i in range(len(fund_list)):
    results.append(nuzhnapomosh_fund_info(fund_list[i]))
    time.sleep(3)
    
df_nuzhnapomosh = pd.DataFrame(
    results,
    columns=[
        "name",
        "description",
        "location",
        "amount_raised",
        "amount_need",
        "to_donate_link",
    ],
)

# устраняем ошибки, которые возникли в процессе выгрузки информации

errors = df_nuzhnapomosh.loc[df_nuzhnapomosh["amount_raised"].isna()].index.values
results = []

for i in errors:
    results.append(nuzhnapomosh_fund_info(fund_list[i]))
    time.sleep(3)

df_nuzhnapomosh.loc[df_nuzhnapomosh['amount_raised'].isna()] = results

project_dir = os.path.abspath(os.path.join(os.getcwd(), '..', '..'))
print(project_dir)

# путь сохранения
output_dir = os.path.join(project_dir, 'data', 'raw')
# имя сохранения
output_file = os.path.join(output_dir, 'df_nuzhnapomosh.csv')
# сохранение df в csv 
df_nuzhnapomosh.to_csv(output_file, index=False)

print(f"nuzhnapomosh dataframe saved to {output_file}")