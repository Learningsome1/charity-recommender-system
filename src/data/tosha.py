import pandas as pd
import os

import requests
from bs4 import BeautifulSoup

def get_links_to_fund_page(path_to_cite: str) -> str:
    """
    Функция возвращает ссылки на страницы фондов на сайте tosha.ru.

    Args:
        path_to_cite (str): ссылка на страницу сайта tosha.ru.

    Returns:
        list: список ссылок на страницы фондов на сайте tosha.ru.
    """

    links = []
    response = requests.get(f"{path_to_cite}")
    soup = BeautifulSoup(response.text).select(".blink")
    j = 0
    while j < len(soup):
        links.append(soup[j]["href"])
        j += 1

    for page_num in range(2, 6):
        response = requests.get(f"{path_to_cite}?PAGEN_1={page_num}")
        soup = BeautifulSoup(response.text).select(".blink")
        j = 0
        while j < len(soup):
            links.append(soup[j]["href"])
            j += 1
    return links

def get_fund_from_page(page_name: str) -> dict:
    """
    Функция возвращает словарь с информацией о фонде.

    Args:
        page_name (str): ссылка на страницу фонда на сайте tosha.ru.

    Returns:
        dict: информация о фонде, ключи: название и описание фонда.
    """

    response = requests.get(f"https://www.tosha.ru{page_name}")
    soup = BeautifulSoup(response.text)
    title = soup.title.text
    if not soup.select_one(".content"):
        content = None
    else:
        content = soup.select_one(".content").text

    return {"name": title, "description": content}

LINK = "https://www.tosha.ru/info/fondy/"
links_to_fund = get_links_to_fund_page(LINK)
df_parts = []

for page in links_to_fund:
    fund = get_fund_from_page(page)
    df_part = pd.DataFrame([fund])
    df_parts.append(df_part)

df = pd.concat(df_parts).reset_index(drop=True)
df.dropna(inplace=True)
df["description"] = (
    df["description"].str.replace(r"[^а-яА-ЯЁё0-9. ]", "", regex=True).str.strip()
)

project_dir = os.path.abspath(os.path.join(os.getcwd(), '..', '..'))
print(project_dir)

# путь сохранения
#output_dir = os.path.join(project_dir, 'data', 'raw')
output_dir = os.path.join('.', 'data', 'raw')
# имя сохранения
output_file = os.path.join(output_dir, 'df_tosha.csv')
# сохранение df в csv 
df.to_csv(output_file, index=False)

print(f"tosha dataframe saved to {output_file}")
