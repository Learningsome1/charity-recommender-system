import pandas as pd
import os

project_dir = os.path.abspath(os.path.join(os.getcwd(), "..", ".."))
print(project_dir)

#data_raw = os.path.join(project_dir, "data", "raw")
data_raw = os.path.join('.', "data", "raw")

print(os.listdir(data_raw))

dataframes = []
assets = ["df_dobromail.csv", "df_nuzhnapomosh.csv", "df_tosha.csv"]

for file_name in assets:
    file_path = os.path.join(data_raw, file_name)
    df = pd.read_csv(file_path)
    dataframes.append(df)

combined_df = pd.concat(dataframes)

# путь сохранения
#output_dir = os.path.join(project_dir, "data", "interim")
output_dir = os.path.join('.', "data", "interim")
# имя сохранения
output_file = os.path.join(output_dir, "df.csv")
# сохранение df в csv
combined_df.to_csv(output_file, index=False)

print(f"Combined dataframe saved to {output_file}")
