import pandas as pd
import os
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup

def dobromail_event_info(fund_num: int) -> str:
    """
    Функция возвращает информацию о благотворительных акциях.

    Args:
        fund_num (int): номер акции на странице dobromail.ru.

    Returns:
        str: название акции, описание, регион, 
             всего собрано средств, осталось собрать, 
             ссылка на проект.
    """

    name = (
        soup.find_all(class_="_0777fd709f")[fund_num]
        .find(class_="c4c98eda51")
        .text.strip()
    )

    description = (
        soup.find_all(class_="_0777fd709f")[fund_num]
        .find(class_="_4f4b5973a6")
        .text.strip()
    )

    location = soup.find_all(class_="_0777fd709f")[fund_num].find_all(
        class_="bc249de543"
    )
    if len(location[0].text.strip()) > 0:
        location = location[0].text.strip()
    else:
        location = location[1].text.strip()

    pl_class = "a7c37cd982 _96be1475d7 _0c3690b223 _301b3aa0b0"
    project_link = "https://dobro.mail.ru" + soup.find_all(class_=pl_class)[f]["href"]

    have = int(
        soup.find_all(class_="c91ccdd1cd")[fund_num]
        .find_all(class_="_9fc67170a5")[0]
        .text.replace("\xa0", "")
        .strip()
    )

    try:
        need = int(
            soup.find_all(class_="c91ccdd1cd")[fund_num]
            .find_all(class_="_9fc67170a5")[1]
            .text.replace("\xa0", "")
            .strip()
        )
    except:
        need = int(
            soup.find_all(class_="c91ccdd1cd")[fund_num]
            .find_all(class_="_9fc67170a5")[0]
            .text.replace("\xa0", "")
            .strip()
        )

    time.sleep(1)

    return name, description, location, have, need, project_link

chrome_options = Options()
chrome_options.add_argument("--headless=new")
driver = webdriver.Chrome("./chromedriver", chrome_options=chrome_options)
LINK = "https://dobro.mail.ru/projects/?recipient=all"
driver.get(LINK)

while True:
    try:
        btn = driver.find_element(By.CLASS_NAME, "b57e7c82f1")
        btn.click()
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(3)
    except:
        break

response = driver.page_source
driver.quit()

soup = BeautifulSoup(response, "html.parser")
fund_count = len(soup.find_all(class_="_0777fd709f"))

results = []

for f in range(fund_count):
    results.append(dobromail_event_info(f))
    
df_dobromail = pd.DataFrame(
    results,
    columns=[
        "name",
        "description",
        "location",
        "amount_raised",
        "amount_need",
        "to_donate_link",
    ],
)

project_dir = os.path.abspath(os.path.join(os.getcwd(), '..', '..'))
print(project_dir)

# путь сохранения
output_dir = os.path.join(project_dir, 'data', 'raw')
# имя сохранения
output_file = os.path.join(output_dir, 'df_dobromail.csv')
# сохранение df в csv 
df_dobromail.to_csv(output_file, index=False)

print(f"dobromail dataframe saved to {output_file}")