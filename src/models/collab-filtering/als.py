import os

import implicit
import mlflow
import numpy as np
import pandas as pd

import metrics
import utils

DATA_FILE = 'user_item_matrix.csv'

# THRESHOLD = 10  # min required num of relevant items per user
# N_RELEVANT_ITEMS_TO_HAVE_IN_TEST_SET_PER_USER = 3
# K = 10  # items to recommend per user

THRESHOLD = 5  # min required num of relevant items per user
N_RELEVANT_ITEMS_TO_HAVE_IN_TEST_SET_PER_USER = 2
K = 10  # items to recommend per user

RANDOM_STATE = 42
ALS_NUM_FACTORS = 5

MLFLOW_EXPERIMENT_ID: str = '3'


# TODO: add cli interface
def main():
    user_item_matrix: pd.DataFrame = pd.read_csv(DATA_FILE)
    print('user-item matrix shape before filtering by threshold:', user_item_matrix.shape)

    user_item_matrix: pd.DataFrame = user_item_matrix[user_item_matrix.sum(axis=1) >= THRESHOLD]
    user_item_matrix = user_item_matrix.reset_index(drop=True)
    print('user-item matrix shape after filtering by threshold:', user_item_matrix.shape)

    train_matrix, test_matrix = utils.leave_k_out(user_item_matrix, k=N_RELEVANT_ITEMS_TO_HAVE_IN_TEST_SET_PER_USER,
                                                  threshold=THRESHOLD, seed=RANDOM_STATE)

    # make sure the total num of relevant items for each user is still the same
    assert np.all(user_item_matrix.sum(axis=1).to_numpy() == (train_matrix.sum(axis=1) + test_matrix.sum(axis=1)).flatten())

    print('training als model...')
    model = implicit.als.AlternatingLeastSquares(
        factors=ALS_NUM_FACTORS, random_state=RANDOM_STATE
    )
    model.fit(train_matrix)

    # рекомендации для всех пользователей
    print('recommending funds/projects to all users using trained model...')
    recommendations = model.recommend(userid=user_item_matrix.index.tolist(), user_items=train_matrix, N=K)[0]

    print('evaluating recommendations on test matrix and logging results to mlflow...')
    mlflow.set_tracking_uri(os.environ.get("MLFLOW_S3_ENDPOINT_URL"))
    with mlflow.start_run(run_name='als-' + utils.get_current_time_utc(),
                          experiment_id=MLFLOW_EXPERIMENT_ID):  # TODO: use K, thr, etc. in run name instead of timestamp
        mlflow.log_param("als_num_factors", ALS_NUM_FACTORS)
        mlflow.sklearn.log_model(model, 'als_models')

        prec = metrics.precision_at_k(test_matrix, recommendations, K)
        print('  prec:', prec)
        mlflow.log_metric('prec', prec)

        recall = metrics.recall_at_k(test_matrix, recommendations, K)
        print('  recall:', recall)
        mlflow.log_metric('recall', recall)

        f1 = metrics.f1_at_k(test_matrix, recommendations, K)
        print('  f1:', f1)
        mlflow.log_metric('f1', f1)

        map_at_k = metrics.map_at_k(test_matrix, recommendations, K)
        print('  map_at_k:', map_at_k)
        mlflow.log_metric('map_at_k', map_at_k)

        mrr = metrics.mrr(test_matrix, recommendations)
        print('  mrr:', mrr)
        mlflow.log_metric('mrr', mrr)

        ndcg_at_k = metrics.ndcg_at_k(test_matrix, recommendations, K)
        print('  ndcg_at_k:', ndcg_at_k)
        mlflow.log_metric('ndcg_at_k', ndcg_at_k)

        ctr = metrics.ctr(test_matrix, recommendations)
        print('  ctr:', ctr)
        mlflow.log_metric('ctr', ctr)


if __name__ == '__main__':
    main()
