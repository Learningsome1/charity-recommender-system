# description of metrics: https://neptune.ai/blog/recommender-systems-metrics

from typing import List

import numpy as np
import scipy


def precision_at_k(test_matrix: scipy.sparse.csr_matrix, recommendations: List[List[int]], k: int):
    precisions = []

    for user_id in range(test_matrix.shape[0]):
        test_items = test_matrix[user_id].indices
        if len(test_items) > 0:
            recommended_items = recommendations[user_id]
            precision = len(set(recommended_items) & set(test_items)) / k
            precisions.append(precision)

    return np.mean(precisions)


def recall_at_k(test_matrix: scipy.sparse.csr_matrix, recommendations: List[List[int]], k: int):
    recalls = []

    for user_id in range(test_matrix.shape[0]):
        test_items = test_matrix[user_id].indices
        if len(test_items) > 0:
            recommended_items = recommendations[user_id]
            recall = len(set(recommended_items[:k]) & set(test_items)) / len(test_items)
            recalls.append(recall)

    return np.mean(recalls)


def f1_at_k(test_matrix: scipy.sparse.csr_matrix, recommendations: List[List[int]], k: int):
    f1_scores = []

    for user_id in range(test_matrix.shape[0]):
        test_items = test_matrix[user_id].indices
        if len(test_items) > 0:
            recommended_items = recommendations[user_id]
            prec = len(set(recommended_items[:k]) & set(test_items)) / k
            rec = len(set(recommended_items[:k]) & set(test_items)) / len(test_items)
            f1 = 2 * prec * rec / (prec + rec) if (prec + rec) > 0 else 0
            f1_scores.append(f1)

    return np.mean(f1_scores)


def map_at_k(test_matrix: scipy.sparse.csr_matrix, recommendations: List[List[int]], k: int):
    average_precisions = []

    for user_id in range(test_matrix.shape[0]):
        test_items = test_matrix[user_id].indices
        if len(test_items) > 0:
            recommended_items = recommendations[user_id]
            precisions = [len(set(recommended_items[:i + 1]) & set(test_items)) / (i + 1) for i, item in
                          enumerate(recommended_items[:k]) if item in test_items]
            if precisions:
                average_precisions.append(np.mean(precisions))

    return np.mean(average_precisions)


def mrr(test_matrix: scipy.sparse.csr_matrix, recommendations: List[List[int]]):
    reciprocal_ranks = []

    for user_id in range(test_matrix.shape[0]):
        test_items = test_matrix[user_id].indices
        if len(test_items) > 0:
            recommended_items = recommendations[user_id]
            for i, item in enumerate(recommended_items):
                if item in test_items:
                    reciprocal_ranks.append(1 / (i + 1))  # ranks are 1-based, so add 1
                    break  # we only care about the rank of the first relevant item

    return np.mean(reciprocal_ranks)


def ndcg_at_k(test_matrix: scipy.sparse.csr_matrix, recommendations: List[List[int]], k: int):
    ndcgs = []

    for user_id in range(test_matrix.shape[0]):
        test_items = test_matrix[user_id].indices
        if len(test_items) > 0:
            recommended_items = recommendations[user_id]
            dcg = 0
            idcg = 0
            for i, rec_item in enumerate(recommended_items, start=1):
                if rec_item in test_items:
                    dcg += 1 / np.log2(
                        i + 1)  # 1 is always in the numerator due to dealing only with relevant items here; i + 1 is to avoid division by 0 if the first item is relevant and, thus, the logarithm is 0
            for i in range(1, min(len(test_items), k) + 1):
                idcg += 1 / np.log2(i + 1)
            ndcgs.append(dcg / idcg if idcg > 0 else 0)

    # print('ngcgs:', ndcgs)
    return np.mean(ndcgs)


def ctr(test_matrix: scipy.sparse.csr_matrix, recommendations: List[List[int]]):
    total_relevant_recommendations = 0
    total_recommendations = 0

    for user_id in range(test_matrix.shape[0]):
        test_items = test_matrix[user_id].indices
        if len(test_items) > 0:
            recommended_items = recommendations[user_id]
            total_relevant_recommendations += len(set(recommended_items) & set(test_items))
            total_recommendations += len(recommended_items)

    return total_relevant_recommendations / total_recommendations if total_recommendations > 0 else 0