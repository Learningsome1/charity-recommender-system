from pathlib import Path
from typing import List

import torch
from sklearn.metrics.pairwise import cosine_similarity

# from transformers import BertTokenizer, BertModel
from sentence_transformers import SentenceTransformer

# from transformers import AutoModel, AutoTokenizer, logging
# import pandas as pd

import utils


PROJ_ROOT_DIR: Path = utils.get_project_root()
# DATA_DIR = PROJ_ROOT_DIR / 'data'
# assert DATA_DIR.exists()
EMBEDDINGS_DIR = PROJ_ROOT_DIR / "models"
assert EMBEDDINGS_DIR.exists()

# logging.set_verbosity_error()  # to suppress huggingface warnings

# tokenizer = AutoTokenizer.from_pretrained("DeepPavlov/rubert-base-cased")
# model = AutoModel.from_pretrained("DeepPavlov/rubert-base-cased")

model = SentenceTransformer(
    "paraphrase-multilingual-MiniLM-L12-v2"
)  # pretty good model for russian lang, better than rubert for our task

# sentences = pd.read_csv('dump_nuzhnapomosh.csv').description
# assert len(sentences) == 602

# load embeddings
# concat_tensors: torch.Tensor = torch.load('nuzhna-embeddings.pt')
concat_tensors: torch.Tensor = torch.load(
    str(EMBEDDINGS_DIR / "nuzhna-embeddings-paraphrase-multilingual-MiniLM-L12-v2.pt")
)
# assert concat_tensors.shape[1] == 768
assert concat_tensors.shape[1] == 384
sentence_embeddings = []
for i in range(concat_tensors.shape[0]):
    sentence_embeddings.append(concat_tensors[i, :])
assert len(sentence_embeddings) > 0

concat_tensors_dobro_mail: torch.Tensor = torch.load(
    str(
        EMBEDDINGS_DIR
        / "dobro-mail-embeddings-paraphrase-multilingual-MiniLM-L12-v2.pt"
    )
)
# assert concat_tensors.shape[1] == 768
assert concat_tensors_dobro_mail.shape[1] == 384
sentence_embeddings_dobro_mail = []
for i in range(concat_tensors_dobro_mail.shape[0]):
    sentence_embeddings_dobro_mail.append(concat_tensors_dobro_mail[i, :])
assert len(sentence_embeddings_dobro_mail) > 0


def compute_embeddings(sentence):
    # Compute embeddings using the Sentence-BERT model
    return torch.Tensor(model.encode(sentence))


# def compute_embeddings(sentence):
#     # Tokenize the sentence
#     inputs = tokenizer(sentence, return_tensors="pt")

#     # Compute embeddings using the BERT model
#     with torch.no_grad():
#         outputs = model(**inputs)

#     # Extract the last hidden state
#     embeddings = outputs.last_hidden_state

#     # Calculate sentence embedding as mean of token embeddings
#     sentence_embedding = torch.mean(embeddings, dim=1).squeeze()

#     return sentence_embedding


def get_ranked_fund_indices(query_sentence: str, kind="funds") -> List[int]:
    assert kind in ("funds", "individuals")

    # top_n = min(top_n, len(sentence_embeddings))

    # Compute the embedding for the query_sentence
    query_embedding = compute_embeddings(query_sentence)

    embeddings = (
        sentence_embeddings if kind == "funds" else sentence_embeddings_dobro_mail
    )

    # Calculate the cosine similarity between the query_embedding and all sentence_embeddings
    # similarities = cosine_similarity(query_embedding.unsqueeze(0).numpy(), [embedding.numpy() for embedding in sentence_embeddings])
    similarities = cosine_similarity(
        query_embedding.unsqueeze(0).numpy(),
        [embedding.numpy() for embedding in embeddings],
    )

    # Get the indices of the top_n most similar sentences
    # top_indices = similarities[0].argsort()[-top_n:][::-1]
    ranked_indices = similarities[0].argsort()[::-1]

    return ranked_indices
