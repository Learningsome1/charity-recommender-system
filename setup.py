from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Charity recommendation system based on the description of charitable non-profit organizations and user interests',
    author='team_orange',
    license='',
)
