image: python:3.9-slim

stages:
  - lint
  - test
  - build

# The $CI_COMMIT_REF_NAME variable in the cache part of the pipeline configuration is 
# used as the cache key. This variable represents the branch or tag name for which the
# pipeline is running. By using $CI_COMMIT_REF_NAME as the cache key, you ensure that 
# each branch or tag has its own separate cache. This setup allows each branch or tag to 
# have its own cache, preventing cache pollution or conflicts between different branches. 
# This is particularly useful when different branches might have different dependencies or 
# build artifacts.
cache:
  key: "$CI_COMMIT_REF_NAME"
  paths:
    # cache these dirs
    - .cache/pip
    - venv/

before_script:
  - python -V
  - which python
  - which pip
  - pwd
  - pip install -q virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - pip install -r requirements.txt
  - echo "Dependencies installed and cached"

linting:
  stage: lint
  # uncomment below rule to run pipeline only upon merge requests to main. Otherwise, it's triggered for 
  # every push to given branch (NB: having this pipeline in main is not enough for it to run on every push to 
  # every branch. Each branch must have this pipeline in repo root for it to be executed)
  
  #rules:
    #- if: '($CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main") || $CI_PIPELINE_SOURCE == "web"'
  script:
    - pip install flake8 black "black[jupyter]" mypy
    # "|| true" below allow job to keep on going even if a given command fails with non-zero exit code, which, for below commands, happens if they merely complain about code style issues and formatting
    - flake8 . --exclude=venv || true  
    - black . --check --exclude=venv || true
    - mypy . --exclude venv || true
  
test_with_pytest:
  stage: test
  rules:
    # only run upon merge requests to main
    - if: '($CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main") || $CI_PIPELINE_SOURCE == "web"'
  script:
    - pip install -q pytest
    - pytest || true
  after_script:
    - echo "pytest tests finished"

build:
  stage: build
  rules:
    # only run upon merge requests to main
    - if: '($CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main") || $CI_PIPELINE_SOURCE == "web"'
  script:
    - python setup.py build
    - python setup.py bdist_wheel
  artifacts:
    paths:
      - dist/  # dir to store generated wheel of our package
