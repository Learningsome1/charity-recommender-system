#!/bin/bash

# Переходим в корневую директорию проекта
cd /home/recsys-dev/charity-recommender-system

echo "I'm running"

# Выполняем git pull для получения обновлений из удаленного репозитория
git pull

rm -rf .dvc/cache
rm data/raw/df_tosha.csv*
rm data/interim/df.csv*

# Запускаем DVC-пайплайн
echo "=== RUNNING REPRO... ==="
dvc repro

# Добавляем обновленные файлы с данными в dvc staging area
#dvc add data/raw/df_tosha.csv
#dvc add data/raw/df_dobromail.csv
#dvc add data/raw/df_nuzhnapomosh.csv

#dvc add data/raw/
#dvc add data/interim/df.csv

# Пушим изменения в S3 хранилище
echo "=== RUNNING DVC PUSH... ==="
dvc push

echo "=== RUNNING GIT ADD... ==="
git add .
git commit -m "Update data"

# Выполняем git push, чтобы отправить изменения в удаленный репозиторий
git push
