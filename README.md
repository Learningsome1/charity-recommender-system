team_orange_project
==============================

Charity recommendation system based on the description of charitable non-profit organizations and user interests

For a walk-through on how to start the TG bot, see *src/tg-bot/README.md*.


# К дз 3

* Коммитов с форматированием кода несколько, например:
  - https://gitlab.com/Learningsome1/charity-recommender-system/-/commit/d5d8ca4ae63dbd20230343f039c5bb0fdb683a36
  - https://gitlab.com/Learningsome1/charity-recommender-system/-/commit/ceb5a27c37c1cd8af73825a8f28da9f71a1493de 
* ТЗ лежит в Wiki (https://gitlab.com/Learningsome1/charity-recommender-system/-/wikis/home) + в docs (файл "Terms_of_reference.md")
* Мердж реквест с конфликтом: https://gitlab.com/Learningsome1/charity-recommender-system/-/merge_requests/11. Остальные мердж реквесты можно найти на соотв. странице.
* Гитлаб-раннер находится на удаленной ВМ.


# Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
